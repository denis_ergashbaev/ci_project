function [fitness, finished,d, threshold]=fitness(population_, n)
%the fitness function corrisponds to the number of duplicates numbers in a
%row + the numbers of duplicate numbers in each column. Intuitivly the
%smaller is this sum the closest to the solution we are. if we found a
%solution the sum will be 0.
finished=false;
threshold=false;
V=ones(n,1);
d=zeros(size(population_,2),1);
%p=factorial(n);
%p=repmat(p,1,n);
t=repmat(45,n,1);
s=[1:1:n];
for z=1:size(population_,2)
board=population_{z};

t=0;
l=0;
% k=0;
% o=0;
t=length(find(histc(board,[1:9],1)==1));
l=length(find(histc(board,[1:9],2)==1));
% for i=1:n
%    l=l+n-length(intersect(s,board(:,i)')); 
%    t=t+n-length(intersect(s,board(i,:)));
%    
% end
%k=sum(abs(board*V-t));

%o=sum(abs(p-prod(board)));
f=t+l;%+k/50;%+1/100*o;
if t==79 &&l==79
    threshold=true;
end
if f==162
    finished=true;
    
else
    fitness(z)=f;
    d(z)=f;
end
end
end
