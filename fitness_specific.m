function [row_fitness, col_fitness]=fitness_specific(board)
row_fitness = zeros(3,1);
col_fitness = zeros(3,1);
for i = 1:3
    start = 3*i - 2;
    row_fitness(i)=length(find(histc(board(start:3*i,:)',[1:9])==1));
    col_fitness(i)=length(find(histc(board(:,start:3*i),[1:9])==1));
end
end
% function [row_fitness, col_fitness]=fitness_specific(board)
% row_fitness = zeros(3,1);
% col_fitness = zeros(3,1);
% for i = 1:3
%     start = 3*i - 2;
%     row_fitness(i)=length(find(histc(board(start:3*i,:)',[1:9])==1));
%     col_fitness(i)=length(find(histc(board(:,start:3*i),[1:9])==1));
% end
% end