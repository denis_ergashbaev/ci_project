function [ new_population_] = crossover(population_,fitness_array_,num_elites,crossover_probability,n,x,y,start_board,population_size )




% s=sum(fitness_array_(:));
%
% probability_unsorted=fitness_array_;
% probability_unsorted=s*ones(size(probability_unsorted))./probability_unsorted;
% s1=sum(probability_unsorted);
% probability_unsorted=probability_unsorted./s1;
% [probability,sorted_indexes] = sort(probability_unsorted,'descend');
%
new_population_=cell(1,100);
s=sum(fitness_array_);
probability_unsorted=fitness_array_./s;
[probability,sorted_indexes] = sort(probability_unsorted,'descend');
population_=population_(sorted_indexes);
new_population_(1:num_elites)=population_(1:num_elites);
%probability(1:num_elites)=[];
%s1=sum(probability);
%probability=probability./s1;
num_childs=length(population_)-num_elites;
parent=cell(2,1);
tmp_population={};
for z=1:num_childs/2
    
    for i=1:2
        
        a=randi(length(population_));
        
        b=randi(length(population_));
        while b==a
            b= randi(length(population_));
        end
        if probability(a)> probability(b)
            parent{i}=population_{a};
            population_(a)=[];
            probability(a)=[];
        else
            parent{i}=population_{b};
            population_(b)=[];
            probability(b)=[];
        end
    end
    %        threshold=rand();
    %        p=probability(1);
    %        for j=1:length(population_)
    %            if p>threshold
    %                 parent{i}=population_{j};
    %                 population_(j)=[];
    %                 probability(j)=[];
    %                 s2=sum(probability);
    %                 probability=probability./s2;
    %                 break;
    %            else
    %                p=p+probability(j);
    %
    %            end
    %        end
    %    end
    if rand()<crossover_probability
        [fitness_row1, fitness_column1] = fitness_specific(parent{1});
        [fitness_row2, fitness_column2] = fitness_specific(parent{2});
        
        offspring1 = [];
        for i = 1:3
            start = 3*i - 2;
            if fitness_row1(i)>fitness_row2(i)
                selparent = parent{1};
            else
                selparent = parent{2};
                
            end
            offspring1(start:start+2,:) = selparent(start:start+2,:);
            
            
        end
        offspring2 = [];
        for i = 1:3
            start = 3*i - 2;
            if fitness_column1(i)>fitness_column2(i)
                selparent = parent{1};
            else
                selparent = parent{2};
                
            end
            offspring2(:,start:start+2) = selparent(:,start:start+2);
            
            
        end
        l=length(tmp_population);
        tmp_population{l+1}=offspring1;
        tmp_population{l+2}=offspring2;
    else
        l=length(tmp_population);
        tmp_population{l+1}=parent{1};
        tmp_population{l+2}=parent{2};
        
    end
    
    
end
s=length(tmp_population);
for h=1:length(tmp_population)
    tmp_population{s+h}=mutate_single(tmp_population{h},0.06,start_board,n,x,y);
end

[fitness_temp,~,~,~] = fitness(tmp_population,n);

[~,indexes]= sort(fitness_temp,'descend');
indexes=indexes(1:num_childs);
new_population_(num_elites+1:end)=tmp_population(indexes);
end

