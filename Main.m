%%%%%%Luis gonna rape us.%
%close all%
% clear all
clc

 load('sudoku_maps-01.mat', 'u01');
% 
%start_board=u01;
%start_board=[1,0,0,0;
%             0,2,0,0;
%             0,0,3,0;
%             0,0,0,4];

% start_board=[0,0,8,3,4,2,9,0,0;
%              0,0,9,0,0,0,7,0,0;
%              4,0,0,0,0,0,0,0,3;
%              0,0,6,4,7,3,2,0,0;
%              0,3,0,0,0,0,0,1,0;
%              0,0,2,8,5,1,6,0,0;
%              7,0,0,0,0,0,0,0,8;
%              0,0,4,0,0,0,1,0,0;
%              0,0,3,6,9,7,5,0,0]
         
start_board=[0, 3, 6, 0, 5, 0, 9, 7, 0;
			      5, 0, 9, 2, 0, 1, 0, 0, 3;
			      0, 1, 0, 0, 6, 9, 8, 2, 0;
			      4, 0, 8, 7, 0, 3, 0, 0, 5;
			      0, 2, 0, 9, 6, 0, 7, 0, 8;
			      7, 0, 3, 0, 8, 0, 0, 4, 2;
			      0, 0, 7, 5, 0, 9, 3, 1, 2;
			      3, 9, 0, 0, 0, 7, 0, 0, 6;
			      0, 5, 4, 0, 3, 6, 0, 0, 8;]         
         

population_size=100;
num_generations=10000;
num_elites=6;
crossover_probability=0.4;
n=9; %size of the board
number_of_flippable_bit=length(find(start_board~=0));
mutation_probability=0.02;
x=3;% size of the subgrid (x)
y=3;% size of the subgrid (y)
n_subgrids=(n*n)/(x*y); %Number of subgrids
subgrids=split_grids(start_board,x,y,n); %divide the initial board into cells

%%create the intial population
for i=1:population_size
    
    initial_popolation{i}=zeros(n,n);
    for j=1:size(subgrids,1)
        for z=1:size(subgrids,2)
        tmp_cell_grid{j,z}=fill_subgrid(subgrids{j,z});
        end
    end
    initial_popolation{i}=cell2mat(tmp_cell_grid);
end
    
%%evaluate the fitness function of each element of the population and save
%%it into an array
population{1}=initial_popolation;
threshold=false;
for i=1:num_generations
    [fitness_array{i},finished,f,threshold] = fitness(population{i},n);
    if finished==true
        break;
    end
%     if threshold==true
%        
%         indexes=find(f==4);
%        
%     end
    i
    %mean(f)
    max(f)
%     fitness_global(i)=sum(fitness_array{i})/initial_population_size;
%     disp(fitness_global(i));
    
  population{i+1} = crossover(population{i},fitness_array{i},num_elites,crossover_probability,n,x,y,start_board,population_size );
%%Mutation



  %  population{i+1}=mutate(population{i+1},mutation_probability,start_board,n,x,y,num_elites);

end
% if ~isempty(indexes)
% solution_to_consider=cell(length(indexes),1)
% if threshold==true && finished == false
%    solution_to_consider=population{i}(indexes)
%    for z=1:length(solution_to_consider)
%        final_board=solution_to_consider{z}
%        for i=1:n
%           if any(~diff(final_board(i,:)))
%              s(i)= find(~diff(final_board(i,:)))
%           end
%        end
%        
%        
%    end
% end
%    
% end
%     
%     
% 
% 
% 
