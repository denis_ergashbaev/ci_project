function [filled_grid]=fill_subgrid(fixed_elements)
%The following function, given a matrix with sum fixed elements diffent
%from 0, fills the places with 0 with a random permutation of the missing
%numbers in the grid and returs the filled grid

    filled_grid=fixed_elements;
    m=size(fixed_elements,1);
    n=size(fixed_elements,2);
    possible_values=1:1:m*n;
    possible_values=setdiff(possible_values,fixed_elements(:)); %find the numbers that are not used yet in the subgrid
    z = randperm(length(possible_values));
    possible_values=possible_values(z); %generate a permutation of the possible values
    tmp=filled_grid(:);% transform the grid to a vector
    tmp(tmp==0)=possible_values; %paste the new values
    filled_grid=reshape(tmp,m,n);
end
