function [splitted_grid]= split_grids(board,x,y,n)
 %divide board into cells of size x by y
splitted_grid=mat2cell(board, ones(n/x,1).*x, ones(n/y,1).*y);
end