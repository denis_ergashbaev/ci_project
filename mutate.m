function [new_population]=mutate(population,mutation_probability,start_board,n,x,y,num_elites)

%%%%TODO::: write the code in a human-readble and comment it. Ask luis
%%%%thats gonna rape us details about the probability. its not actually
%%%%right but maybe it can work. More explenations in the next episode!

new_population=cell(1,100);
new_population(1:num_elites)=population(1:num_elites);

population(1:num_elites)=[];
fixed_values=split_grids(start_board,x,y,n);

for z=1:length(population)
considered_individual=population{z};

consider_inividual_cell=split_grids(considered_individual,x,y,n);
for i=1:n
    for j=1:n
       if start_board(i,j)==0 %if the bit we want to mutate is not fixed
             if rand() < mutation_probability
                cell_to_change_x=ceil(i/x);
                cell_to_change_y=ceil(j/y);
                fix_cell=fixed_values{cell_to_change_x,cell_to_change_y};
            
                g=consider_inividual_cell{cell_to_change_x,cell_to_change_y};
                x_b=mod(i,x);
                if x_b==0
                     x_b=x;
                end
                    y_b=mod(j,y);
                if y_b==0
                     y_b=y;
                end
                bit_to_flip=g(x_b,y_b);
                consider_inividual_cell{cell_to_change_x,cell_to_change_y} =flip_bit(fix_cell,g,bit_to_flip);
        
       end      
    end
end
new_individual=cell2mat(consider_inividual_cell);
end
new_population{num_elites+z}=new_individual;
end
end