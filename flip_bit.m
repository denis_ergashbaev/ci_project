function [ flipped_grid] = flip_bit(fixed_values, grid_to_flip, bit_to_flip )
%given a x by y grid and the fixed values for that grid, it flips a bit
%with coordinates x_b,x_y with another random bit that is not fixed
             
             filppable_bits=setdiff(grid_to_flip,fixed_values)';
             filppable_bits=setdiff(filppable_bits,bit_to_flip);
             new_bit=filppable_bits(randi(length(filppable_bits)));
             grid_to_flip(grid_to_flip==new_bit)=NaN;
             grid_to_flip(grid_to_flip==bit_to_flip)=new_bit;
             grid_to_flip(isnan(grid_to_flip))=bit_to_flip;
             flipped_grid=grid_to_flip;
end

