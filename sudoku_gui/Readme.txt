Sudoku! is the ultimate GUI for all Sudoku lovers. It really removes the messy scribbles; simply left- or right-click to either select or discard the hints. Hints are generated on-the-go by eliminating the invalid candidates after every move.

The GUI loads both *.txt and *.mat files. In this new package, two solvers are included- sudoku_solve resolves naked singles, hidden singles, hidden pairs and locked candidates; sudoku_solveguess employs a guessing strategy to reach a possible solution. Also included in this new package is sudoku_mini which really helps to simplify gameplay (see screenshot). For the advanced users, you may even develop your own functions and launch them from the GUI (see Readme.txt for more details). Like the previous version, a Record&Recall option lets you store the present board for later retrieval. And lastly, to keep frustration levels within healthy bounds, I have also added an Undo button to reverse the moves that you last made. :)

A must-have for all Sudoku enthusiasts, amateurs and pros alike! Get one now and play Sudoku like you never did before!

If you like the GUI, please comment below; otherwise (e.g., if you find a suspicious bug), send an email to me (weechiat@gmail.com). :)

Essential files: sudoku_config.txt, sudoku_gui.m, sudoku_guicb.m, sudoku_makemap.m
Accompanying files: sudoku_mini.m, sudoku_solve.m, sudoku_solveguess.m, sudoku_maps-01.mat, sudoku_maps-02.mat, sudoku_maps-03.mat, sudoku_maps-z01.mat, Readme.txt

** General instructions **
1. To launch, type "sudoku_gui" in the workspace.
2. Left-click to select, right-click to discard. The usual suspects (common digits contained in the row, coloumn or box) are discarded after every selection.
3. Click on Record to store the present board (with hints). Clicking Recall will retrieve the last stored board.
4. Click Undo to undo your moves. You can undo up to 10 moves.
5. Choose to print with or without hints with the print options in the menu.
6. The GUI loads both *.txt and *.mat files but saves only in *.txt format. In *.txt format, blanks are represented by 0's. Hints are not retained in the saved files. Hints are, however, retained in Record&Recall.
7. To use sudoku_mini, click on the keypad (on the right) to turn on/off the corresponding digit. Typing the digit on the keyboard does the same thing too.

** Additional features **
The GUI will register its auxiliary functions (listed in sudoku_config.txt) every time it is launched. These functions have the function syntax:
> fig = function_name('gui', {map, tee})
where map is a 9x9 matrix and tee is a 9x9 cell containing the possible candidates. See sudoku_mini as an example.

** Limitations **
1. The sudoku_mini GUI will not be updated every time a move is made in the main GUI. Call on the function again if you require it.
2. The function sudoku_solveguess may run indefinitely (though not always) if the given map admits several solutions.
