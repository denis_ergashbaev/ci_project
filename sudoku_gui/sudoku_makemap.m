function fig = sudoku_makemap(map,tee)
% Make map for sudoku.
% [fig] = sudoku_makemap(map,[tee])

% Set constants.
c1 = [0.2 0.2 0.2 0 0 0 -0.2 -0.2 -0.2];
c2 = [-0.2 0 0.2 -0.2 0 0.2 -0.2 0 0.2];

% Set figure.
h0 = figure('Color',[1 1 1], ...
   'Name','Sudoku!, mb@', ...
   'Units','points', ...
   'Position',[100 100 400 400], ...
   'PaperPositionMode','auto', ...
   'PaperType','A4', ...
   'MenuBar','none', ...
   'ToolBar','none');

% Set axes.
axes('Parent',h0, ...
   'Units','points', ...
   'Position',[0 0 400 400], ...
   'XLim',[0 10], ...
   'YLim',[0 10], ...
   'Visible','off');
for k1 = 1:9, for k2 = 1:9, rectangle('Position',[k1-0.5,9.5-k2,1,1]); end, end
for k1 = [1 4 7], for k2 = [1 4 7], rectangle('Position',[k1-0.5,7.5-k2,3,3],'LineWidth',3); end, end

% Populate map.
switch nargin,
case 1
   for k1 = 1:9
      for k2 = 1:9
         if map(k1,k2) > 0
            text(k2,10-k1,num2str(map(k1,k2)),'FontSize',36,'HorizontalAlignment','center');
         end
      end
   end
case 2
   for k1 = 1:9
      for k2 = 1:9
         if map(k1,k2) > 0
            text(k2,10-k1,num2str(map(k1,k2)),'FontSize',36,'HorizontalAlignment','center');
         elseif ~isempty(tee{k1,k2})
            for k3 = 1:9
               if any(tee{k1,k2} == k3)
                  text(k2+c2(k3),10-k1+c1(k3),num2str(k3),'FontSize',8,'HorizontalAlignment','center');
               end
            end
         end
      end
   end
end

if nargout > 0, fig = h0; end
