function fig = sudoku_mini(action,varargin)
% >> sudoku_mini is an auxiliary function for sudoku_gui
% Click on the keypad (on the right) to turn on/off the corresponding
% digit. Typing the digit on the keyboard does the same thing too.

switch action,
   
case 'gui'
   
   % Set inputs.
   map = varargin{1};
   tee = varargin{2};
   
   % Set constants.
   m1 = [
      0			0			0.5625
      0			0			0.625
      0			0			0.6875
      0			0			0.75
      0			0			0.8125
      0			0			0.875
      0			0			0.9375
      0			0			1
      0			0.0625	1
      0			0.125		1
      0			0.1875	1
      0			0.25		1
      0			0.3125	1
      0			0.375		1
      0			0.4375	1
      0			0.5		1
      0			0.5625	1
      0			0.625		1
      0			0.6875	1
      0			0.75		1
      0			0.8125	1
      0			0.875		1
      0			0.9375	1
      0			1			1
      0.0625	1			1
      0.125		1			0.9375
      0.1875	1			0.875
      0.25		1			0.8125
      0.3125	1			0.75
      0.375		1			0.6875
      0.4375	1			0.625
      0.5		1			0.5625
      0.5625	1			0.5
      0.625		1			0.4375
      0.6875	1			0.375
      0.75		1			0.3125
      0.8125	1			0.25
      0.875		1			0.1875
      0.9375	1			0.125
      1			1			0.0625
      1			1			0
      1			0.9375	0
      1			0.875		0
      1			0.8125	0
      1			0.75		0
      1			0.6875	0
      1			0.625		0
      1			0.5625	0
      1			0.5		0
      1			0.4375	0
      1			0.375		0
      1			0.3125	0
      1			0.25		0
      1			0.1875	0
      1			0.125		0
      1			0.0625	0
      1			0			0
      0.9375	0			0
      0.875		0			0
      0.8125	0			0
      0.75		0			0
      0.6875	0			0
      0.625		0			0
      0.5625	0			0];
   m2 = [236 233 216]/255;
   c1 = [0.25 0.25 0.25 0 0 0 -0.25 -0.25 -0.25]-0.125;
   c2 = [-0.25 0 0.25 -0.25 0 0.25 -0.25 0 0.25]-0.125;
   c3 = transpose([1 2 3; 4 5 6; 7 8 9]);
   c4 = {[1 0 0.8],[0.8 1 0],[0 0.8 1],[0.5 1 0.5],[0.5 0.5 1],[1 0.5 0.5],[1 0.8 0.5],[0.5 1 0.8],[0.8 0.5 1]};
   
   % Set figure.
   h0 = figure('Color',m2, ...
      'Colormap',m1, ...
      'Name','Sudoku!, mb@', ...
      'Units','points', ...
      'Position',[120 120 393 300], ...
      'MenuBar','none', ...
      'ToolBar','none', ...
      'KeyPressFcn','sudoku_mini(''keypressfcn'',get(gcbf,''UserData''))');
   
   % Set axes.
   axes('Parent',h0, ...
      'Units','points', ...
      'Position',[0 0 393 300], ...
      'XLim',[0 13.1], ...
      'YLim',[0 10], ...
      'Visible','off');
   for k1 = 1:9, for k2 = 1:9, rectangle('Position',[k1-0.5,9.5-k2,1,1]); end, end
   for k1 = [1 4 7], for k2 = [1 4 7], rectangle('Position',[k1-0.5,7.5-k2,3,3],'LineWidth',3); end, end
   
   % Populate map.
   hndl.h1 = zeros(9,9,9);
   for k1 = 1:9
      for k2 = 1:9
         if map(k1,k2) > 0
            text(k2,10-k1,num2str(map(k1,k2)),'Color',[0.5 0.5 0.5],'FontSize',24,'HorizontalAlignment','center');
         elseif ~isempty(tee{k1,k2})
            for k3 = 1:9
               if any(tee{k1,k2} == k3)
                  hndl.h1(k1,k2,k3) = rectangle('Position',[k2+c2(k3),10-k1+c1(k3),0.25,0.25], ...
                     'FaceColor',c4{k3},'Visible','off');
               end
            end
         end
      end
   end
   
   % Draw keypad.
   rectangle('Position',[10.15,6.85,2.3,2.3],'LineWidth',1);
   rectangle('Position',[10.2,6.9,2.2,2.2],'LineWidth',1);
   for k1 = 1:3
      for k2 = 1:3
         u1                 = 10.3+0.7*(k1-1);
         u2                 = 8.4-0.7*(k2-1);
         hndl.h2(c3(k1,k2)) = rectangle('Position',[u1,u2,0.6,0.6],'FaceColor',c4{c3(k1,k2)},'Visible','off');
         hndl.h3(c3(k1,k2)) = rectangle('Position',[u1,u2,0.6,0.6],'FaceColor',m2);
         text(u1+0.3,u2+0.3,num2str(c3(k1,k2)),'FontSize',12,'FontWeight','bold','HorizontalAlignment','center', ...
            'ButtonDownFcn','sudoku_mini(''buttondownfcn'',get(gcbf,''UserData''))');
      end
   end
   
   % Save to hObject.
   hObject.hndl = hndl;
   set(h0,'UserData',hObject);
   
   if nargout > 0, fig = h0; end
   
case 'keypressfcn'
   
   % Set inputs.
   hObject = varargin{1};
   
   d = str2num(get(gcbf,'CurrentCharacter'));
   if isempty(d), return, end
   
   if d == 0
      for k = 1:9
         set(hObject.hndl.h1(:,:,k),'Visible','off');
         set(hObject.hndl.h2(k),'Visible','off');
         set(hObject.hndl.h3(k),'Visible','on');
      end
   elseif d >= 1 & d <= 9
      switch get(hObject.hndl.h2(d),'Visible'),
      case 'off'
         set(hObject.hndl.h1(:,:,d),'Visible','on');
         set(hObject.hndl.h2(d),'Visible','on');
         set(hObject.hndl.h3(d),'Visible','off');
      case 'on'
         set(hObject.hndl.h1(:,:,d),'Visible','off');
         set(hObject.hndl.h2(d),'Visible','off');
         set(hObject.hndl.h3(d),'Visible','on');
      end
   end
   
case 'buttondownfcn'
   
   % Set inputs.
   hObject = varargin{1};
   
   d = str2num(get(gcbo,'String'));
   
   switch get(hObject.hndl.h2(d),'Visible'),
   case 'off'
      set(hObject.hndl.h1(:,:,d),'Visible','on');
      set(hObject.hndl.h2(d),'Visible','on');
      set(hObject.hndl.h3(d),'Visible','off');
   case 'on'
      set(hObject.hndl.h1(:,:,d),'Visible','off');
      set(hObject.hndl.h2(d),'Visible','off');
      set(hObject.hndl.h3(d),'Visible','on');
   end
   
end
