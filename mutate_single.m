function [new_individual]=mutate_single(considered_individual,mutation_probability,start_board,n,x,y)

fixed_values=split_grids(start_board,x,y,n);

consider_inividual_cell=split_grids(considered_individual,x,y,n);
for i=1:n
    for j=1:n
       if start_board(i,j)==0 %if the bit we want to mutate is not fixed
             if rand() < mutation_probability
                cell_to_change_x=ceil(i/x);
                cell_to_change_y=ceil(j/y);
                fix_cell=fixed_values{cell_to_change_x,cell_to_change_y};
            
                g=consider_inividual_cell{cell_to_change_x,cell_to_change_y};
                x_b=mod(i,x);
                if x_b==0
                     x_b=x;
                end
                    y_b=mod(j,y);
                if y_b==0
                     y_b=y;
                end
                bit_to_flip=g(x_b,y_b);
                consider_inividual_cell{cell_to_change_x,cell_to_change_y} =flip_bit(fix_cell,g,bit_to_flip);
        
       end      
    end
    end
end
new_individual=cell2mat(consider_inividual_cell);
end
% 
% function [new_individual]=mutate_single(considered_individual,mutation_probability,start_board,n,x,y)
% 
% 
% 
% board_to_mutate=considered_individual-start_board;
% if rand() < mutation_probability
%     t=randi(3);
%     s=randi(3);
%     cell_to_change=board_to_mutate(3*t - 2:t+2,3*s - 2:s+2)
%     P=find(cell_to_change>0)
%    
% end
% end